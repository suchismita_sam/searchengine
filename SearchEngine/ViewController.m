//
//  ViewController.m
//  SearchEngine
//
//  Created by Click Labs134 on 10/6/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

#import "ViewController.h"

NSMutableArray *pizzaMenu;
NSMutableArray *result;
UITableViewCell *cell;
@interface ViewController ()
@property (strong, nonatomic) IBOutlet UIButton *orderButton;
@property (strong, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (strong, nonatomic) IBOutlet UISearchBar *searchEngine;
@property (strong, nonatomic) IBOutlet UITableView *listOfNames;

@end

@implementation ViewController

@synthesize backgroundImage;
@synthesize searchEngine;
@synthesize listOfNames;
@synthesize orderButton;

- (void)viewDidLoad {
    [super viewDidLoad];
    pizzaMenu=[NSMutableArray arrayWithObjects:@"Zesty Chicken",@"Chicken Fiesta",@"Chicken Golden Delight",@"Chicken Barbequeue",@"Spicy Chicken",@"chicken mexican",@"keema do pyaza",@"Cheese n barbequeue chicken",@"Margerita",@"Cheese and tomato pizza",@"Spicy delight",@"double cheese margerita",@"fresh veggie",@"country special",@"farmhouse",@"5 pepper",@"peppy paneer",@"mexican green wave",@"deluxe veggie",@"gourmet",nil];
    result=[NSMutableArray arrayWithArray:pizzaMenu];
    [listOfNames reloadData];
    //result=[NSMutableArray arrayWithCapacity:[pizzaMenu count]];

    // Do any additional setup after loading the view, typically from a nib.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [result count];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    NSPredicate *resultPredicate=[NSPredicate predicateWithFormat: @"SELF contains[c] %@",searchText];
    result=[NSMutableArray arrayWithArray:[pizzaMenu filteredArrayUsingPredicate:resultPredicate]];
    [listOfNames reloadData];
    //listOfNames.hidden=YES;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell=[tableView dequeueReusableCellWithIdentifier:@"cellReuse"];
    cell.backgroundColor=[UIColor blueColor];
    cell.textLabel.text=result[indexPath.row];
    cell.textColor=[UIColor whiteColor];
    
    return cell;
}



-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}


-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(editingStyle==UITableViewCellEditingStyleDelete)
    {
        [pizzaMenu removeObjectAtIndex:indexPath.row];
        [listOfNames deleteRowsAtIndexPaths:[NSMutableArray arrayWithObjects:indexPath, nil ] withRowAnimation:UITableViewRowAnimationFade];

        }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIAlertView *messageAlert = [[UIAlertView alloc]
                                 initWithTitle:@"ORDER" message:@"You've selected a your order" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    // Display Alert Message
    [messageAlert show];
    
}

-(void)deleteRow:(UIButton *)sender

{
    [listOfNames setEditing:YES animated:YES];
}

- (IBAction)order:(id)sender {
    UIAlertView *messageAlert2 = [[UIAlertView alloc]
                                 initWithTitle:@"ORDER" message:@"You've selected a your order" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    // Display Alert Message
    [messageAlert2 show];
}






- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
